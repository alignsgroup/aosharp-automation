﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Character.State
{
    public enum CharacterWieldedWeapon
    {
        Bandaid = 1337,
        Invalid = 0,
        Fists = 1,
        Bow = 12,
        Smg = 20,
        Edged1H = 34,
        Blunt1H = 66,
        Edged2H = 130,
        Blunt2H = 258,
        Piercing = 514,
        Pistol = 1028,
        AssaultRifle = 2052,
        Rifle = 4100,
        Shotgun = 8196,
        Grenade = 32772,
        PistolAndShotgun = 9220,
        HeavyWeapons = 65540,
        PistolAndAssaultRifle = 3076,
        Blunt1HAndEnergy = 16450,
        Energy = 16386,
        Blunt1HAndEdged1H = 98,
        Blunt1HAndPiercing = 578,
        Edged1HAndPiercing = 546,
    }
}
